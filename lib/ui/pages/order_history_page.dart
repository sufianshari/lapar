part of 'pages.dart';

class OrderHistoryPage extends StatefulWidget {
  @override
  _OrderHistoryPageState createState() => _OrderHistoryPageState();
}

class _OrderHistoryPageState extends State<OrderHistoryPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransactionCubit, TransactionState>(
      // ignore: missing_return
      builder: (_, state) {
        if (state is TransactionLoaded) {
          if (state.transaction.length == 0) {
            return IlustrationPage(
                title: 'Ouch Hungry',
                subtitle: 'Seems you have not\nordered any food yet',
                picturePath: 'assets/food.png',
                buttonTitle1: 'Find Foods',
                buttonTap1: () {});
          } else {
            // *  selain itu munculkan layar transaksi
            double listItemWidht =
                MediaQuery.of(context).size.width - 2 * defaultMargin;
            return GeneralPage(
              title: 'Order History',
              subtitle: 'Your order history',
              child: Container(
                width: double.infinity,
                color: Colors.white,
                child: Column(
                  children: [
                    CustomTabBarText(
                      titles: ['In Progress', 'Past Orders'],
                      selectedIndex: selectedIndex,
                      onTap: (index) {
                        setState(() {
                          selectedIndex = index;
                        });
                      },
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Builder(builder: (_) {
                      List<Transaction> transaction = (selectedIndex == 0)
                          ? state.transaction
                              .where((element) =>
                                  element.status ==
                                      TransactionStatus.on_delivery ||
                                  element.status == TransactionStatus.pending)
                              .toList()
                          : state.transaction
                              .where((element) =>
                                  element.status ==
                                      TransactionStatus.delivered ||
                                  element.status == TransactionStatus.cancelled)
                              .toList();
                      return Column(
                        children: transaction
                            .map(
                              (e) => Padding(
                                padding: EdgeInsets.fromLTRB(
                                    defaultMargin, 0, defaultMargin, 16),
                                child: GestureDetector(
                                  //? jadi ketika di hisoty masih pending maka kita bisa klik untuk membayar
                                  onTap: () async {
                                    if (e.status == TransactionStatus.pending) {
                                      await launch(e.paymentUrl);
                                    }
                                  },
                                  child: OrderListItem(
                                      transaction: e, itemWidht: listItemWidht),
                                ),
                              ),
                            )
                            .toList(),
                      );
                    }),
                    SizedBox(
                      height: 60,
                    ),
                  ],
                ),
              ),
            );
          }
        } else {
          Center(
            child: loadingIndicator,
          );
        }
      },
    );
  }
}
